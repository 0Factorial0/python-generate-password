def clear():
    import os
    #clear screen
    test_os = os.name
    if test_os == "nt":
        os.system('cls')
    elif test_os == "posix":
        os.system('clear')
    else:
        pass
    
def generate_strong_password(length=12):
    import secrets
    import string
    
    #define charset
    characters = string.ascii_letters + string.digits + string.punctuation
    password = secrets.choice(string.ascii_uppercase) + \
               secrets.choice(string.ascii_lowercase) + \
               secrets.choice(string.digits) + \
               secrets.choice(string.punctuation)
    
    #make up password
    password += ''.join(secrets.choice(characters) for _ in range(length - 4))

    #shuffle the password to make it more random
    password_list = list(password)
    secrets.SystemRandom().shuffle(password_list)
    password = ''.join(password_list)

    return password

def start():

    #print out
    print(generate_strong_password(16))

start()